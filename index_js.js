const posts = [
    {
        id: 1,
        name: "Vincent van Gogh",
        username: "vincey1853",
        location: "Zundert, Netherlands",
        avatar: "images/avatar-vangogh.jpg",
        post: "images/post-vangogh.jpg",
        comment: "just took a few mushrooms lol",
        likes: 21
    },
    {
        id: 2,
        name: "Gustave Courbet",
        username: "gus1819",
        location: "Ornans, France",
        avatar: "images/avatar-courbet.jpg",
        post: "images/post-courbet.jpg",
        comment: "i'm feelin a bit stressed tbh",
        likes: 4
    },
        {
        id: 3,
        name: "Joseph Ducreux",
        username: "jd1735",
        location: "Paris, France",
        avatar: "images/avatar-ducreux.jpg",
        post: "images/post-ducreux.jpg",
        comment: "gm friends! which coin are YOU stacking up today?? post below and WAGMI!",
        likes: 152
    }
];

// render sections using posts object.
for (let postInfo of posts) {
    // clone last section and append new after last;
    let sectionsAmount = document.getElementsByTagName('section').length;
    let lastSection = document.getElementsByTagName('section')[sectionsAmount - 1];
    let newSection = lastSection.cloneNode(true);
    newSection.hidden = false;
    newSection.id = postInfo.id;
    lastSection.after(newSection);

    // Edit section personal properties.
    let avatarImage = newSection.getElementsByClassName('avatar-img')[0];
    avatarImage.src = postInfo.avatar;
    avatarImage.alt = `avatar of ${postInfo.name}`;

    let name = newSection.getElementsByClassName('name')[0];
    name.innerText = postInfo.name;

    let location = newSection.getElementsByClassName('location')[0];
    location.innerText = postInfo.location;

    let postImage = newSection.getElementsByClassName('post-img')[0];
    postImage.src = postInfo.post;
    postImage.alt = `posted image of ${postInfo.name}`;

    let likes = newSection.getElementsByClassName('likes')[0];
    likes.innerText = `${postInfo.likes} likes`;

    let username = newSection.getElementsByClassName('username')[0];
    username.innerText = `${postInfo.username}`;

    let comments = newSection.getElementsByClassName('comment')[0];
    comments.innerText = `${postInfo.comment}`;

    // Increment like with like-btn and post-img doubleclick.
    let likeBtn = newSection.getElementsByClassName('like-btn')[0];
    postImage.addEventListener("dblclick", addLike);
    likeBtn.addEventListener('click', addLike);
}

// Adding like on doubleckick on post-img or clicking on like-btn.
// Of course in real world I would send that info to backend, but for learning purposes i'm changing values in place.
function addLike(event) {
    // Section that raised event.
    let section;
    for (let element of event.path) {
        if (element.tagName == 'SECTION') { 
            section = element;
        }
    }
    // Manipulating likes amount.
    // check if section id in local storage
    let sectionID = Number(section.id);

    // get displayed number of likes.
    let likesElem = section.getElementsByClassName('likes')[0];
    let likesText = likesElem.innerText;
    //searching for index of whitespace that divides number of likes and 'likes' string.
    let whiteSpace = likesText.search(/ /); 
    likes = Number(likesText.slice(0, whiteSpace));
    // like-btn.
    let likeBtn = section.getElementsByClassName('like-btn')[0];

    // If like triggered save info in storage.
    // If like triggered for the 1 time. 
    if (!localStorage.getItem(sectionID)) {
        // +1 like in html.
        likesElem.innerText = `${likes + 1} likes`;
        // Register in storage that section was liked;
        localStorage.setItem(`${sectionID}`, '1');
        // change like-btn style.
        likeBtn.style.filter = 'opacity(0.25) drop-shadow(0 0 0 blue)';
    }
    // If like triggered on liked section decrement amount of likes and delete from storage.
    else {
        // -1 like in html.
        likesElem.innerText = `${likes - 1} likes`;
        // Delete info that section was liked.
        localStorage.removeItem(`${sectionID}`);
        // Set btn style to default.
        likeBtn.style.filter = '';
    }
}