# Description
My goal for this learning project was to recreate instagram posts feed. 

# WARNING!
I did that project before knowing about innenHTML property, so you will see adding properties to elements was done by as manually as it propably gets.

## Skills.
- Created an html structure and added some styling via CSS.
- Posts individual properties are rendered by JS using information from js object.
- Added like-button that increases amount of likes or returns to default. I added button functionality in place, meaning
that changes was registered in html. For this project it seemed enough.
